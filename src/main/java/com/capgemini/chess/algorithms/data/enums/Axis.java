package com.capgemini.chess.algorithms.data.enums;

import com.capgemini.chess.algorithms.data.Coordinate;

public enum Axis {
	
	HORIZONTAL{
		@Override
		public int shift(Coordinate from, Coordinate to) {
			return to.getX() - from.getX();
		}
	},
	VERTICAL{
		@Override
		public int shift(Coordinate from, Coordinate to) {
			return to.getY() - from.getY();
		}
	},
	SLASH{
		@Override
		public int shift(Coordinate from, Coordinate to) {
			return to.getY() - from.getY();
		}
	},
	BACKSLASH{
		@Override
		public int shift(Coordinate from, Coordinate to) {
			return to.getY() - from.getY();
		}
	};
	
	
	
	/**
	 * Gives distance measured on specific axis.
	 * In case of slash or backslash distance is >0 when piece moves upward (Y is increases) 
	 */
	public abstract int shift(Coordinate from, Coordinate to);
}
