package com.capgemini.chess.algorithms.data.generated;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.Move;
import com.capgemini.chess.algorithms.data.enums.BoardState;
import com.capgemini.chess.algorithms.data.enums.Color;
import com.capgemini.chess.algorithms.data.enums.Piece;

/**
 * Board representation.
 * Board objects are generated based on move history.
 * 
 * @author Michal Bejm
 *
 */
public class Board {
	
	public static final int SIZE = 8;
	public static final int BLACK_PAWNS_START_Y = SIZE - 2;
	public static final int WHITE_PAWNS_START_Y = 1;
	
	private Piece[][] pieces = new Piece[SIZE][SIZE];
	private List<Move> moveHistory = new ArrayList<>();
	private BoardState boardState;	

	public List<Move> getMoveHistory() {
		return moveHistory;
	}

	public Piece[][] getPieces() {
		return pieces;
	}

	public BoardState getBoardState() {
		return boardState;
	}

	public void setBoardState(BoardState boardState) {
		this.boardState = boardState;
	}

	
	/**
	 * Sets chess piece on board based on given coordinates
	 * 
	 * @param piece chess piece
	 * @param board chess board
	 * @param coordinate given coordinates
	 */
	public void setPieceAt(Piece piece, Coordinate coordinate) {
		pieces[coordinate.getX()][coordinate.getY()] = piece;
	}
	
	/**
	 * Gets chess piece from board based on given coordinates
	 * 
	 * @param coordinate given coordinates
	 * @return chess piece
	 */
	public Piece getPieceAt(Coordinate coordinate) {
		return pieces[coordinate.getX()][coordinate.getY()];
	}
	
	/**
	 * @return coordinates of all pieces on the board
	 */
	public List<Coordinate> getCoordinatesOfPieces(){
		LinkedList<Coordinate> listOfPieces = new LinkedList<>();
		
		for(int X = 0; X < Board.SIZE; X++){
			for(int Y = 0; Y < Board.SIZE; Y++){
				if(pieces[X][Y] != null){
					listOfPieces.add(new Coordinate(X, Y));
				}
			}
		}
		return listOfPieces;
	}
	
	/**
	 * @param color color of pieces to find
	 * @return coordinates of all pieces on the board with color
	 */
	public List<Coordinate> getCoordinatesOfPiecesByColor(Color color){
		return this.getCoordinatesOfPieces()
				.stream()
				.filter(coordinate -> this.getPieceAt(coordinate).getColor() == color)
				.collect(Collectors.toCollection(LinkedList::new));
	}
}
