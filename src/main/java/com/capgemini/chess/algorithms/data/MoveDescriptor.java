package com.capgemini.chess.algorithms.data;

import com.capgemini.chess.algorithms.data.enums.Axis;
import com.capgemini.chess.algorithms.data.enums.Color;
import com.capgemini.chess.algorithms.data.enums.Piece;
import com.capgemini.chess.algorithms.data.generated.Board;

public class MoveDescriptor {

	private Coordinate from;
	private Coordinate to;
	private Axis moveAxis;

	public Coordinate getFrom() {
		return from;
	}

	public Coordinate getTo() {
		return to;
	}
	
	public Axis getMoveAxis() {
		return moveAxis;
	}

	public MoveDescriptor(Coordinate from, Coordinate to) {
		this.from = from;
		this.to = to;
		this.moveAxis = defineAxis(from, to);
	}

	public int absShiftX() {
		return Math.abs(shiftX());
	}

	public int absShiftY() {
		return Math.abs(shiftY());
	}

	public int shiftX() {
		return to.getX() - from.getX();
	}

	public int shiftY() {
		return to.getY() - from.getY();
	}

	
	private Axis defineAxis(Coordinate from, Coordinate to) {
		if (to.getX() - from.getX() == 0) {
			return Axis.VERTICAL;
		}
		if (to.getY() - from.getY() == 0) {
			return Axis.HORIZONTAL;
		}
		if (to.getX() - from.getX() == to.getY() - from.getY()) {
			return Axis.SLASH;
		}
		if (to.getX() - from.getX() == -(to.getY() - from.getY())) {
			return Axis.BACKSLASH;
		}
		return null;
	}
 
	public boolean isMoveForward(Color color) {
		int distance = moveAxis.shift(from, to);
		switch (color) {
		case WHITE:
			return distance > 0;
		case BLACK:
			return distance < 0;
		}
		return false;
	}

	public boolean isAtStartingPosition(Piece piece) {
		switch (piece.getColor()) {
		case WHITE:
			return from.getY() == Board.WHITE_PAWNS_START_Y;
		case BLACK:
			return from.getY() == Board.BLACK_PAWNS_START_Y;
		}
		return false;
	}

	public boolean isCoordinateOnRoad(Coordinate coordinateToCheck) {
		Axis coordinateToCheckAxis = defineAxis(this.from, coordinateToCheck);
		if(coordinateToCheckAxis != moveAxis || moveAxis == null){
			return false;
		}
		int distanceToCoordinate = coordinateToCheckAxis.shift(this.from, coordinateToCheck);
		
		return isCoordinateToCheckCloserThanDestination(distanceToCoordinate) &&
				isSameDirection(distanceToCoordinate);
	}
	
	private boolean isCoordinateToCheckCloserThanDestination(int distanceToCoordinate){
		return Math.abs(distanceToCoordinate) < Math.abs(moveAxis.shift(from, to));
	}

	private boolean isSameDirection(int distanceToCoordinate){
		return moveAxis.shift(from, to) < 0 && distanceToCoordinate < 0 ||
				moveAxis.shift(from, to) > 0 && distanceToCoordinate > 0;
	}
	
	
	
	
}
