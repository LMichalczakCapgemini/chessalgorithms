package com.capgemini.chess.algorithms.data.enums;

/**
 * Chess piece color
 * 
 * @author Michal Bejm
 *
 */
public enum Color {
	WHITE {
		@Override
		public Color oppositeColor() {
			return Color.BLACK;
			}
	}, 
	BLACK {
		@Override
		public Color oppositeColor() {
			return Color.WHITE;
		}
	};
	
	public abstract Color oppositeColor();
}
