package com.capgemini.chess.algorithms.moveValidators;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.generated.Board;

public class QueenMoveValidator extends MoveValidator {

	public QueenMoveValidator(Coordinate from, Coordinate to, Board board) {
		super(from, to, board);
	}

	@Override
	public boolean isMoveAllowed() {
		return moveDescriptor.absShiftX() == 0 ||
				moveDescriptor.absShiftY() == 0 ||
				moveDescriptor.absShiftX() == moveDescriptor.absShiftY();
	}

	@Override
	public boolean isMovePossible(){
		return super.isMovePossible() && !isAnyObstacle();
	}


}
