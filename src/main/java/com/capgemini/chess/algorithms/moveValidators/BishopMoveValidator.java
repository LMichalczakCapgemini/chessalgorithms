package com.capgemini.chess.algorithms.moveValidators;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.generated.Board;

public class BishopMoveValidator extends MoveValidator {

	public BishopMoveValidator(Coordinate from, Coordinate to, Board board) {
		super(from, to, board);
	}
 
	@Override
	public boolean isMoveAllowed() {
		return moveDescriptor.absShiftX() == moveDescriptor.absShiftY();
	}
	
	@Override
	public boolean isMovePossible() {
		return super.isMovePossible() && !isAnyObstacle();
	}
	
}
