package com.capgemini.chess.algorithms.moveValidators;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.generated.Board;

public class MoveValidatorFactory {
	
	public MoveValidator CreateMoveValidator(Coordinate from, Coordinate to, Board board){
		MoveValidator moveValidator = null;

		switch (board.getPieceAt(from).getType()) {
		case KING: 
			moveValidator = new KingMoveValidator(from, to, board);
			break;
		case QUEEN: 
			moveValidator = new QueenMoveValidator(from, to, board);
			break;
		case BISHOP:
			moveValidator = new BishopMoveValidator(from, to, board);
			break;
		case KNIGHT:
			moveValidator = new KnightMoveValidator(from, to, board);
			break;
		case ROOK: 
			moveValidator = new RookMoveValidator(from, to, board);
			break;
		case PAWN: 
			moveValidator = new PawnMoveValidator(from, to, board);
			break;
		}
		
		return moveValidator;
	}
	
}
