package com.capgemini.chess.algorithms.moveValidators;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.enums.MoveType;
import com.capgemini.chess.algorithms.data.generated.Board;
import com.capgemini.chess.algorithms.implementation.exceptions.InvalidMoveException;

public class KingMoveValidator extends MoveValidator {
	
	public KingMoveValidator(Coordinate from, Coordinate to, Board board) {
		super(from, to, board);
	}

	@Override
	public boolean isMoveAllowed() {
		return moveDescriptor.absShiftX() <= 1 && 
				moveDescriptor.absShiftY() <= 1;
	}

	@Override
	public MoveType getTypeOfMove() throws InvalidMoveException {
		if(isCastling()){
			return MoveType.CASTLING;
		}
		return super.getTypeOfMove();
	}

	private boolean isCastling(){
		//TODO implement
		return false;
	}

}
