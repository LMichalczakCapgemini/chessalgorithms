package com.capgemini.chess.algorithms.moveValidators;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.generated.Board;

public class KnightMoveValidator extends MoveValidator {

	public KnightMoveValidator(Coordinate from, Coordinate to, Board board) {
		super(from, to, board);
	}

	@Override
	public boolean isMoveAllowed() {
		return moveDescriptor.absShiftX() == 2 && moveDescriptor.absShiftY() == 1 ||
				moveDescriptor.absShiftX() == 1 && moveDescriptor.absShiftY() == 2;
	}

	
}
