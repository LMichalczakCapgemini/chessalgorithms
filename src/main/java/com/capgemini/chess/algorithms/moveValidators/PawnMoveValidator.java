package com.capgemini.chess.algorithms.moveValidators;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.Move;
import com.capgemini.chess.algorithms.data.MoveDescriptor;
import com.capgemini.chess.algorithms.data.enums.Color;
import com.capgemini.chess.algorithms.data.enums.MoveType;
import com.capgemini.chess.algorithms.data.enums.Piece;
import com.capgemini.chess.algorithms.data.enums.PieceType;
import com.capgemini.chess.algorithms.data.generated.Board;
import com.capgemini.chess.algorithms.implementation.exceptions.InvalidMoveException;

public class PawnMoveValidator extends MoveValidator {

	public PawnMoveValidator(Coordinate from, Coordinate to, Board board) {
		super(from, to, board);
	}

	@Override
	public boolean isMoveAllowed() {
		if(!super.isMoveAllowed()){
			return false;
		}
		Piece movingPawn = board.getPieceAt(moveDescriptor.getFrom());
		if (!moveDescriptor.isMoveForward(movingPawn.getColor())) {
			return false;
		}
		if (!moveDescriptor.isAtStartingPosition(movingPawn)) {
			return isAllowedNextPawnMove();
		} else {
			return isAllowedFirstPawnMove();
		}
	} 

	private boolean isAllowedNextPawnMove() {
		return moveDescriptor.absShiftY() == 1 && moveDescriptor.absShiftX() <= 1;
	}

	private boolean isAllowedFirstPawnMove() {
		return moveDescriptor.absShiftY() == 1 && moveDescriptor.absShiftX() <= 1
				|| moveDescriptor.absShiftY() == 2 && moveDescriptor.absShiftX() == 0;
	}

	// TODO pod skosem tylko atak // w isPossible

	@Override
	public boolean isMovePossible() {
		if (isEnPassantMove() && !isEnPassantPossible()) {
				return false;
		}
		return super.isMovePossible() && !isAnyObstacle();
	}

	@Override
	public MoveType getTypeOfMove() throws InvalidMoveException  {
		if (isEnPassantMove()) {
			return MoveType.EN_PASSANT;
		}
		if(isCapture()){
			return MoveType.CAPTURE;
		}
		if(isAttack()){
			return MoveType.ATTACK;
		}
		throw new InvalidMoveException();
	}

	//move left or right at null
	private boolean isEnPassantMove() {
		return moveDescriptor.absShiftX() == 1 && board.getPieceAt(moveDescriptor.getTo()) == null;
	}
	
	private boolean isCapture(){
		return moveDescriptor.absShiftX() == 1 && board.getPieceAt(moveDescriptor.getTo()) != null;
	}
	
	private boolean isAttack(){
		return moveDescriptor.absShiftX() == 0 && 
				moveDescriptor.absShiftY() <= 2 &&
				board.getPieceAt(moveDescriptor.getTo()) == null;
	}

	private boolean isEnPassantPossible() {
		Coordinate coordinateBehindAfterMove = calculateCoordinateBehindAfterMove();
		Color enemyColor = board.getPieceAt(moveDescriptor.getFrom()).getColor().oppositeColor();
		
		
		return isEnemyPawnBehind(coordinateBehindAfterMove, enemyColor) &&
				wasLastMoveInPlaceByTwo(coordinateBehindAfterMove);
	}
	
	private Coordinate calculateCoordinateBehindAfterMove(){
		int xBehind = moveDescriptor.getTo().getX();
		int yBehind = moveDescriptor.getTo().getY();
		
		switch (board.getPieceAt(moveDescriptor.getFrom()).getColor()) {
		case WHITE:
			yBehind--;
			break;
		case BLACK:
			yBehind++;
			break;
		}
		return new Coordinate(xBehind, yBehind);
	}

	private boolean isEnemyPawnBehind(Coordinate behind, Color enemyColor) {
		Piece pieceBehind = board.getPieceAt(behind);
		if (pieceBehind == null) {
			return false;
		}

		return pieceBehind.getColor() == enemyColor && pieceBehind.getType() == PieceType.PAWN;
	}
	// czy ostatniu ruch byl na behind i czy byl o dwa
	private boolean wasLastMoveInPlaceByTwo(Coordinate place){
		int lastIndex = board.getMoveHistory().size() - 1;
		Move lastMove = board.getMoveHistory().get(lastIndex);
		MoveDescriptor lastMoveDescriptor = new MoveDescriptor(lastMove.getFrom(), lastMove.getTo());
		
		return lastMove.getTo().getX() == place.getX() &&
				lastMove.getTo().getY() == place.getY() && 
				lastMoveDescriptor.absShiftY() == 2;		
	}
}
