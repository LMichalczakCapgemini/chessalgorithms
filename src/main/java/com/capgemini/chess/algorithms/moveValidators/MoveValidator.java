package com.capgemini.chess.algorithms.moveValidators;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.MoveDescriptor;
import com.capgemini.chess.algorithms.data.enums.Color;
import com.capgemini.chess.algorithms.data.enums.MoveType;
import com.capgemini.chess.algorithms.data.enums.Piece;
import com.capgemini.chess.algorithms.data.generated.Board;
import com.capgemini.chess.algorithms.implementation.exceptions.InvalidMoveException;

public abstract class MoveValidator {

	protected MoveDescriptor moveDescriptor;
	protected Board board;

	public MoveValidator(Coordinate from, Coordinate to, Board board) {
		super();
		this.moveDescriptor = new MoveDescriptor(from, to);
		this.board = board;
	}

	public boolean isMoveAllowed(){
		return moveDescriptor.getMoveAxis() != null;
	}

	public boolean isMovePossible() {
		return !isAllyAtDestination();
	} 

	private boolean isAllyAtDestination() {
		Piece pieceAtDestination = board.getPieceAt(moveDescriptor.getTo());
		if (pieceAtDestination == null) {
			return false;
		}
		Color allyColor = board.getPieceAt(moveDescriptor.getFrom()).getColor();
		return allyColor == pieceAtDestination.getColor();
	}

	protected boolean isAnyObstacle() {
		return board.getCoordinatesOfPieces().stream()
				.anyMatch(coordinate -> moveDescriptor.isCoordinateOnRoad(coordinate)); 
	}

	public MoveType getTypeOfMove() throws InvalidMoveException{
		Piece pieceAtDestination = board.getPieceAt(moveDescriptor.getTo());
		if (pieceAtDestination == null) {
			return MoveType.ATTACK;
		}
		return MoveType.CAPTURE;
	}
	
}
