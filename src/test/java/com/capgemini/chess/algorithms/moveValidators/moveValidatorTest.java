package com.capgemini.chess.algorithms.moveValidators;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.enums.MoveType;
import com.capgemini.chess.algorithms.data.enums.Piece;
import com.capgemini.chess.algorithms.data.generated.Board;
import com.capgemini.chess.algorithms.moveValidators.MoveValidator;
import com.capgemini.chess.algorithms.moveValidators.MoveValidatorFactory;

public class moveValidatorTest {

	private MoveValidatorFactory moveValidatorFactory = new MoveValidatorFactory();

	@Test
	public void shouldBeNotPossibleWhenObstackle() {
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_KING, new Coordinate(4, 0));
		board.setPieceAt(Piece.WHITE_QUEEN, new Coordinate(1, 3));
		board.setPieceAt(Piece.BLACK_ROOK, new Coordinate(3, 1));
		MoveValidator moveValidator = moveValidatorFactory.CreateMoveValidator(new Coordinate(1, 3),
				new Coordinate(4, 0), board);

		boolean isPossible = moveValidator.isMovePossible();

		assertFalse(isPossible);
	}

	@Test
	public void shouldPossibleWhenNoObstackle() {
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_KING, new Coordinate(3, 1));
		board.setPieceAt(Piece.BLACK_ROOK, new Coordinate(5, 5));
		board.setPieceAt(Piece.BLACK_BISHOP, new Coordinate(4, 0));
		board.setPieceAt(Piece.WHITE_QUEEN, new Coordinate(1, 3));
		board.setPieceAt(Piece.BLACK_ROOK, new Coordinate(0, 4));
		MoveValidator moveValidator = moveValidatorFactory.CreateMoveValidator(new Coordinate(1, 3),
				new Coordinate(3, 1), board);

		boolean isPossible = moveValidator.isMovePossible();

		assertTrue(isPossible);
	}

	@Test
	public void isAllayAtDestination() {
		// given
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_KING, new Coordinate(4, 0));
		board.setPieceAt(Piece.BLACK_QUEEN, new Coordinate(3, 0));
		MoveValidator sut = moveValidatorFactory.CreateMoveValidator(new Coordinate(4, 0), new Coordinate(3, 0), board);

		// when
		boolean isPossible = sut.isMovePossible();

		// then
		assertFalse(isPossible);
	}

	@Test
	public void noAllayAtDestination() {
		// given
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_KING, new Coordinate(4, 0));
		board.setPieceAt(Piece.BLACK_QUEEN, new Coordinate(5, 5));
		MoveValidator moveValidator = moveValidatorFactory.CreateMoveValidator(new Coordinate(4, 0),
				new Coordinate(3, 0), board);

		// when
		boolean isPossible = moveValidator.isMovePossible();

		// then
		assertTrue(isPossible);
	}

	@Test
	public void shouldBeAllowedMoveKing() {
		// given
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_KING, new Coordinate(4, 0));
		MoveValidator moveValidator = moveValidatorFactory.CreateMoveValidator(new Coordinate(4, 0),
				new Coordinate(3, 1), board);

		// when
		boolean isAllowed = moveValidator.isMoveAllowed();

		// then
		assertTrue(isAllowed);
	}
	
	@Test
	public void shouldBeAllowedMovePawnTwoFromSart() {
		// given
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_PAWN, new Coordinate(6, 6));
		MoveValidator moveValidator = moveValidatorFactory.CreateMoveValidator(new Coordinate(6, 6),
				new Coordinate(6, 4), board);

		// when
		boolean isAllowed = moveValidator.isMoveAllowed();

		// then
		assertTrue(isAllowed);
	}
	
	@Test
	public void shouldBeAllowedMovePawnNextCapture() {
		// given
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_PAWN, new Coordinate(5, 5));
		MoveValidator moveValidator = moveValidatorFactory.CreateMoveValidator(new Coordinate(5, 5),
				new Coordinate(5, 4), board);

		// when
		boolean isAllowed = moveValidator.isMoveAllowed();

		// then
		assertTrue(isAllowed);
	}
	
	@Test
	public void shouldBeAllowedMovePawnAttack() {
		// given
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_PAWN, new Coordinate(4, 4));
		board.setPieceAt(Piece.BLACK_PAWN, new Coordinate(4, 4));
		MoveValidator sut = moveValidatorFactory.CreateMoveValidator(new Coordinate(4, 4),
				new Coordinate(3, 3), board);

		// when
		boolean isAllowed = sut.isMoveAllowed();

		// then
		assertTrue(isAllowed);
	}
	
	@Test
	public void shouldNotBeAllowedMovePawnBack() {
		// given
		Board board = new Board();
		board.setPieceAt(Piece.BLACK_PAWN, new Coordinate(4, 4));
		MoveValidator moveValidator = moveValidatorFactory.CreateMoveValidator(new Coordinate(4, 4),
				new Coordinate(4, 5), board);

		// when
		boolean isAllowed = moveValidator.isMoveAllowed();

		// then
		assertFalse(isAllowed);
	}
	
	

	
}
