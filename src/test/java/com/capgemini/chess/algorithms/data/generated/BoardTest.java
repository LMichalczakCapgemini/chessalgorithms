package com.capgemini.chess.algorithms.data.generated;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.enums.Color;
import com.capgemini.chess.algorithms.data.enums.Piece;
import com.capgemini.chess.algorithms.data.generated.Board;

public class BoardTest {

	@Test
	public void getAllCoordinatesOfPiecesOnBoard() {
		//given
		Board sut = new Board();
		sut.setPieceAt(Piece.BLACK_KING, new Coordinate(7, 0));
		sut.setPieceAt(Piece.WHITE_ROOK, new Coordinate(3, 2));
		sut.setPieceAt(Piece.WHITE_PAWN, new Coordinate(0, 7));
		
		//when
		List<Coordinate> coordinatesList = sut.getCoordinatesOfPieces();
		
		//then
		assertTrue(coordinatesList.size() == 3);
		assertTrue(coordinatesList.contains(new Coordinate(7, 0)));
		assertTrue(coordinatesList.contains(new Coordinate(3, 2)));
		assertTrue(coordinatesList.contains(new Coordinate(0, 7)));
	}
	
	@Test
	public void getAllCoordinatesOfSpecificColorPiecesOnBoard() {
		//given
		Board sut = new Board();
		sut.setPieceAt(Piece.BLACK_KING, new Coordinate(7, 0));
		sut.setPieceAt(Piece.WHITE_ROOK, new Coordinate(3, 2));
		sut.setPieceAt(Piece.WHITE_PAWN, new Coordinate(0, 7));
		
		//when
		List<Coordinate> coordinatesListWhite = sut.getCoordinatesOfPiecesByColor(Color.WHITE);
		List<Coordinate> coordinatesListBlack = sut.getCoordinatesOfPiecesByColor(Color.BLACK);
		
		//then
		assertTrue(coordinatesListWhite.size() == 2);
		assertTrue(coordinatesListBlack.size() == 1);
		assertTrue(coordinatesListBlack.contains(new Coordinate(7, 0)));
		assertTrue(coordinatesListWhite.contains(new Coordinate(3, 2)));
		assertTrue(coordinatesListWhite.contains(new Coordinate(0, 7)));
	}

}
