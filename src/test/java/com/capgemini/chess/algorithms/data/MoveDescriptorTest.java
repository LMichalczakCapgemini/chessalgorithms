package com.capgemini.chess.algorithms.data;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.chess.algorithms.data.Coordinate;
import com.capgemini.chess.algorithms.data.MoveDescriptor;
import com.capgemini.chess.algorithms.data.enums.Color;
import com.capgemini.chess.algorithms.data.enums.Piece;

public class MoveDescriptorTest {

	@Test
	public void shouldDecideIfCoordinateIsOnRoad(){
		//given 
		Coordinate from = new Coordinate(3, 3);
		MoveDescriptor sutHorizontal = new MoveDescriptor(from, new Coordinate(7, 3));
		MoveDescriptor sutHorizontalMin = new MoveDescriptor(from, new Coordinate(0, 3));
		MoveDescriptor sutVertical = new MoveDescriptor(from, new Coordinate(3, 7));
		MoveDescriptor sutVerticalMin = new MoveDescriptor(from, new Coordinate(3, 0));
		MoveDescriptor sutSlash = new MoveDescriptor(from, new Coordinate(6, 6));
		MoveDescriptor sutSlashMin = new MoveDescriptor(from, new Coordinate(0, 0));
		MoveDescriptor sutBackslash = new MoveDescriptor(from, new Coordinate(0, 6));
		MoveDescriptor sutBackslashMin = new MoveDescriptor(from, new Coordinate(6, 0));
		
		//when
		boolean isOnRoadHorizontial = sutHorizontal.isCoordinateOnRoad(new Coordinate(6, 3));
		boolean isOnRoadHorizontialMin = sutHorizontalMin.isCoordinateOnRoad(new Coordinate(1, 3));
		boolean isOnRoadVertical = sutVertical.isCoordinateOnRoad(new Coordinate(3, 6));
		boolean isOnRoadVerticalMin = sutVerticalMin.isCoordinateOnRoad(new Coordinate(3, 1));
		boolean isOnRoadSlash = sutSlash.isCoordinateOnRoad(new Coordinate(5, 5));
		boolean isOnRoadSlashMin = sutSlashMin.isCoordinateOnRoad(new Coordinate(1, 1));
		boolean isOnRoadBackslash = sutBackslash.isCoordinateOnRoad(new Coordinate(2, 4));
		boolean isOnRoadBackslashMin = sutBackslashMin.isCoordinateOnRoad(new Coordinate(5, 1));
		boolean wrongAxis = sutBackslashMin.isCoordinateOnRoad(new Coordinate(0, 3));
		boolean noneAxis = sutBackslashMin.isCoordinateOnRoad(new Coordinate(0, 1));
		boolean wrongDistance = sutSlash.isCoordinateOnRoad(new Coordinate(7, 7));
		boolean wrongDirection = sutSlashMin.isCoordinateOnRoad(new Coordinate(4, 4));

		//then
		assertTrue(isOnRoadHorizontial);
		assertTrue(isOnRoadHorizontialMin);
		assertTrue(isOnRoadVertical);
		assertTrue(isOnRoadVerticalMin);
		assertTrue(isOnRoadSlash);
		assertTrue(isOnRoadSlashMin);
		assertTrue(isOnRoadBackslash);
		assertTrue(isOnRoadBackslashMin);
		assertFalse(wrongAxis);
		assertFalse(noneAxis);
		assertFalse(wrongDistance);
		assertFalse(wrongDirection);
	}

	
	@Test
	public void decideIfMoveForward(){
		MoveDescriptor sutWhiteForward = new MoveDescriptor(new Coordinate(1, 1), new Coordinate(1, 3));
		MoveDescriptor sutWhiteBack = new MoveDescriptor(new Coordinate(1, 1), new Coordinate(1, 0));
		MoveDescriptor sutBlackForward = new MoveDescriptor(new Coordinate(6, 6), new Coordinate(6, 5));
		MoveDescriptor sutBlackBack = new MoveDescriptor(new Coordinate(6, 6), new Coordinate(6, 7));
		
		boolean isWhiteMoveForward = sutWhiteForward.isMoveForward(Color.WHITE);
		boolean isWhiteNotMoveForward = sutWhiteBack.isMoveForward(Color.WHITE);
		boolean isBlackMoveForward = sutBlackForward.isMoveForward(Color.BLACK);
		boolean isBlackNotMoveForward = sutBlackBack.isMoveForward(Color.BLACK);

		assertTrue(isWhiteMoveForward);
		assertFalse(isWhiteNotMoveForward);
		assertTrue(isBlackMoveForward);
		assertFalse(isBlackNotMoveForward);
	}
	
	@Test 
	public void decideIfPawnAtStartinPosition(){
		MoveDescriptor sutWhiteStart = new MoveDescriptor(new Coordinate(1, 1), new Coordinate(1, 3));
		MoveDescriptor sutWhiteNext = new MoveDescriptor(new Coordinate(2, 2), new Coordinate(3, 3));
		MoveDescriptor sutBlackStart = new MoveDescriptor(new Coordinate(6, 6), new Coordinate(6, 5));
		MoveDescriptor sutBlacknext = new MoveDescriptor(new Coordinate(3, 3), new Coordinate(2, 2));
		
		boolean isWhiteMoveStart = sutWhiteStart.isAtStartingPosition(Piece.WHITE_PAWN);
		boolean isWhiteNotMoveNext = sutWhiteNext.isAtStartingPosition(Piece.WHITE_PAWN);
		boolean isBlackMoveStart = sutBlackStart.isAtStartingPosition(Piece.BLACK_PAWN);
		boolean isBlackNotMoveNext = sutBlacknext.isAtStartingPosition(Piece.BLACK_PAWN);

		assertTrue(isWhiteMoveStart);
		assertFalse(isWhiteNotMoveNext);
		assertTrue(isBlackMoveStart);
		assertFalse(isBlackNotMoveNext);
	}
}
